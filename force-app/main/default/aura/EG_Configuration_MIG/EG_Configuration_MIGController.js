({
	doInit : function(component, event, helper) {
		console.log('Iniciando generador...');
     
        var newConfig = {sobjectType:"EG_Settings__mdt", DeveloperName: component.get("v.eg_name")};
        var action=component.get("c.getEvents");
        if (!component.get("v.eg_name")) {
            component.set("v.incorrecto",true);
            helper.showToast('Event Generator', 'Remember to set for name the component in Edit Page!!', 'error');
        }
        action.setCallback(this, function(a){
            var state = a.getState();
            if (state === "SUCCESS") {
                var resultado = a.getReturnValue();
                component.set("v.events", resultado);
                if (resultado.length > 0) {
	                component.set("v.event",resultado[0].api_name);
                    helper.gFields(component);
                }
            }  else if (state === "ERROR") {
                console.log(a);        	
            }
            
			component.set("v.eg_config",newConfig); 
        });
        $A.enqueueAction(action);
	},
    addField : function(component, event, helper) {
        var config = component.get("v.eg_config");
		var newField = {sobjectType:"EG_Field__mdt", Event__c: config.Id}; 
        var lista = component.get("v.eg_fields")
        lista.push(newField);
        component.set("v.eg_fields",lista);
	},
    changeEvent: function (component, event, helper) {
        component.set("v.eg_fields",[]);
        console.log("Cambio de Evento");
    },
    salvar: function (component, event, helper) {
        if (true) {
        	helper.showToast('Event Generator', 'Configuration succesfuly saved. You can now hide configuration', 'success');
        	component.set("v.saved", true);
            var saveEVT = component.getEvent("EG_Save_Event"); 
			saveEVT.fire();
        }
    },
    removeField: function (component, event, helper) {
        var position = event.getParam("position");
        console.log('Eliminando campo '+position);
        var x = component.get("v.eg_fields");
        x.splice(position, 1);
        component.set("v.eg_fields",x);
        
    }
})