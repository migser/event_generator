({
	showToast: function (titulo, msg, tipo) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": tipo,
            "title": titulo,
            "message": msg
        });
        toastEvent.fire();
    },
    gFields : function (component) {
      var evento = component.get("v.event");
      var action=component.get("c.getFields");
      action.setParams({"event": evento});
      action.setCallback(this, function(a){
            var state = a.getState();
            if (state === "SUCCESS") {
                var resultado = a.getReturnValue();
                component.set("v.event_fields", resultado);
                
            }  else if (state === "ERROR") {
                console.log(a);        	
            }
        });
        $A.enqueueAction(action);
    }
})