({
    doInit : function(component, event, helper) {
		var e = component.get("v.event_fields");
        component.set("v.index",0); 
        var f = e[0];
        f.used = true;
 
        component.set("v.old_index",0);
        component.set("v.event_fields",e);
        
        console.log('--------------------');
            console.log('Seleccionado: 0');
        for (var j = 0; j < e.length; j++) {
            
            console.log('Elemento '+j+': '+e[j].field.API_Name__c+' '+e[j].used);
		}
	},
	remove : function(component, event, helper) {
		var removeEVT = component.getEvent("EG_RemoveField_MIG"); 
        removeEVT.setParams({"position": component.get("v.position")});
	    removeEVT.fire();
	},
    changeField : function(component, event, helper) {
        var e = component.get("v.event_fields");
        var i = component.get("v.index");
        var o = component.get("v.old_index");
        var f = e[i];
        
        f.used = true;
        e[i] = f;
        var of = e[o];
        of.used=false;
        
        component.set("v.old_index",i);
        component.set("v.event_fields",e);
        console.log('--------------------');
            console.log('Seleccionado: '+i);
        for (var j = 0; j < e.length; j++) {
            
            console.log('Elemento '+j+': '+e[j].field.API_Name__c+' '+e[j].used);
		}
	}
})