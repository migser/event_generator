public with sharing class  EG_Controller_MIG {
	@AuraEnabled
    public static List<Map<String,Object>> getEvents() {
        List<Map<String,Object>> resultado = new List<Map<String,Object>>();
        
        for(Schema.SObjectType objTyp : Schema.getGlobalDescribe().Values()){
   			String name = objTyp.getDescribe().getName();
   			String label = objTyp.getDescribe().getLabel();
            if (name.endsWith('__e')) {
                Map<String,Object> e = new Map<String,Object>();
                e.put('api_name', name);
                e.put('label', label);
               List<Object> l = new List<Object>();
                e.put('fields', l);
                resultado.add(e);
            }   
		 }
        
        return resultado;
    }
    @AuraEnabled
    public static List<Map<String,Object>> getFields(String event) {
    	Map <String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(event).getDescribe().fields.getMap();
        List<Map<String,Object>> resultado = new List<Map<String,Object>>();
		for(Schema.SObjectField sfield : fieldMap.Values())
		{   
            Schema.DescribeFieldResult d = sfield.getDescribe();
            if (d.isCustom()) {
             EG_Field_Settings__c  f = new EG_Field_Settings__c (API_Name__c = d.getName(), Field_Name__c = d.getLabel(),Label__c = d.getLabel(), Data_Type__c = d.getType().name());
             Map<String,Object> x = new Map<String,Object>();
         	 x.put('used', false);
             x.put('field',f);
           	 resultado.add(x);
            }
        }
        return resultado;
        
    }
}